package com.example.photondemo.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.photondemo.ui.main.models.HighSchoolDetail
import com.example.photondemo.ui.main.utls.HighSchoolRepository

class HighSchoolDetailViewModel(private val highSchoolRepository: HighSchoolRepository) : ViewModel() {

    fun getHighSchoolDetails(dbn:String): LiveData<List<HighSchoolDetail>> {
        return highSchoolRepository.getHighSchoolDetails(dbn)
    }
}