package com.example.photondemo.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.photondemo.R
import com.example.photondemo.databinding.FragmentDetailBinding
import com.example.photondemo.ui.main.adapters.SchoolDetailAdapter
import com.example.photondemo.ui.main.services.HighSchoolService
import com.example.photondemo.ui.main.utls.HighSchoolRepository
import com.example.photondemo.ui.main.utls.ViewModelFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class HighSchoolDetailFragment(private val dbn:String) : Fragment() {
    private lateinit var binding: FragmentDetailBinding
    companion object {
        fun newInstance(dbn:String) = HighSchoolDetailFragment(dbn)
    }

    private lateinit var viewModel: HighSchoolDetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val retrofit = Retrofit.Builder()
            .baseUrl("https://data.cityofnewyork.us/resource/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val service = retrofit.create(HighSchoolService::class.java)
        val repository = HighSchoolRepository(service)
        viewModel = ViewModelProvider(this, ViewModelFactory(repository))[HighSchoolDetailViewModel::class.java]
        viewModel.getHighSchoolDetails(dbn).observe(this, Observer { highSchools ->
            if(highSchools.isNullOrEmpty()){
                Toast.makeText(requireContext(),"no such dbn or service error",Toast.LENGTH_SHORT).show()
                parentFragmentManager.popBackStackImmediate()
            }else{
                val adapter = SchoolDetailAdapter(highSchools)
                binding.schoolDetailList.layoutManager = LinearLayoutManager(context).apply {
                    this.orientation = LinearLayoutManager.VERTICAL
                }
                binding.schoolDetailList.adapter = adapter
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_detail, container, false)
        return binding.root
    }

}