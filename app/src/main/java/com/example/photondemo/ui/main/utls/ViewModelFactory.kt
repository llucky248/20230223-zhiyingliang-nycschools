package com.example.photondemo.ui.main.utls

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.photondemo.ui.main.HighSchoolDetailViewModel
import com.example.photondemo.ui.main.HighSchoolViewModel

class ViewModelFactory(private val repository: HighSchoolRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(HighSchoolViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return HighSchoolViewModel(repository) as T
        }else if (modelClass.isAssignableFrom(HighSchoolDetailViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return HighSchoolDetailViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}