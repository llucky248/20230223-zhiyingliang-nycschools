package com.example.photondemo.ui.main.services

import com.example.photondemo.ui.main.models.HighSchool
import com.example.photondemo.ui.main.models.HighSchoolDetail
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface HighSchoolService {

    //@Headers("X-App-Token: YOUR_APP_TOKEN_HERE")
    @GET("s3k6-pzi2.json")
    fun getHighSchools(): Call<List<HighSchool>>

    @GET("f9bf-2cp4.json")
    fun getHighSchoolsDetail(@Query("dbn") dbn: String): Call<List<HighSchoolDetail>>
}