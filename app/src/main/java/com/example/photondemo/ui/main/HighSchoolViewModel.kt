package com.example.photondemo.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.photondemo.ui.main.models.HighSchool
import com.example.photondemo.ui.main.utls.HighSchoolRepository

class HighSchoolViewModel(private val highSchoolRepository: HighSchoolRepository) : ViewModel() {

    fun getHighSchools(): LiveData<List<HighSchool>> {
        return highSchoolRepository.getHighSchools()
    }
}