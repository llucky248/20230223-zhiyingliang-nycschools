package com.example.photondemo.ui.main.utls

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.photondemo.ui.main.models.HighSchool
import com.example.photondemo.ui.main.models.HighSchoolDetail
import com.example.photondemo.ui.main.services.HighSchoolService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HighSchoolRepository(private val highSchoolService: HighSchoolService) {
    val TAG: String = this.javaClass.simpleName

    fun getHighSchools(): LiveData<List<HighSchool>> {
        val highSchoolsLiveData = MutableLiveData<List<HighSchool>>()

        highSchoolService.getHighSchools().enqueue(object : Callback<List<HighSchool>> {
            override fun onResponse(call: Call<List<HighSchool>>, response: Response<List<HighSchool>>) {
                if (response.isSuccessful) {
                    highSchoolsLiveData.value = response.body()
                } else {
                    Log.e(TAG, "Request failed with code ${response.code()}")
                }
            }

            override fun onFailure(call: Call<List<HighSchool>>, t: Throwable) {
                Log.e(TAG, "Request failed", t)
            }
        })

        return highSchoolsLiveData
    }

    fun getHighSchoolDetails(dbn:String): LiveData<List<HighSchoolDetail>> {
        val highSchoolDetailLiveData = MutableLiveData<List<HighSchoolDetail>>()

        highSchoolService.getHighSchoolsDetail(dbn).enqueue(object : Callback<List<HighSchoolDetail>> {
            override fun onResponse(call: Call<List<HighSchoolDetail>>, response: Response<List<HighSchoolDetail>>) {
                if (response.isSuccessful) {
                    highSchoolDetailLiveData.value = response.body()
                } else {
                    Log.e(TAG, "Request failed with code ${response.code()}")
                }
            }

            override fun onFailure(call: Call<List<HighSchoolDetail>>, t: Throwable) {
                Log.e(TAG, "Request failed", t)
            }
        })

        return highSchoolDetailLiveData
    }
}
