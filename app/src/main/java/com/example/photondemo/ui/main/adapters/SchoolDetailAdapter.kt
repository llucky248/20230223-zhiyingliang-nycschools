package com.example.photondemo.ui.main.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.photondemo.R
import com.example.photondemo.databinding.ItemSchoolDetailBinding
import com.example.photondemo.ui.main.models.HighSchoolDetail

class SchoolDetailAdapter(private val schools: List<HighSchoolDetail>) : RecyclerView.Adapter<SchoolDetailAdapter.SchoolViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemSchoolDetailBinding>(inflater, R.layout.item_school_detail, parent, false)
        return SchoolViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        val school = schools[position]
        holder.binding.textviewSchoolName.text = school.schoolName
        holder.binding.textviewSchoolMathSat.text = school.satMathAvgScore
        holder.binding.textviewSchoolWSat.text = school.satWritingAvgScore
        holder.binding.textviewSchoolRSat.text = school.satCriticalReadingAvgScore
    }

    override fun getItemCount(): Int {
        return schools.size
    }
    class SchoolViewHolder(val binding: ItemSchoolDetailBinding) : RecyclerView.ViewHolder(binding.root)
}
