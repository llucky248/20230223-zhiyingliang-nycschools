package com.example.photondemo.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.photondemo.R
import com.example.photondemo.databinding.FragmentMainBinding
import com.example.photondemo.ui.main.adapters.SchoolAdapter
import com.example.photondemo.ui.main.services.HighSchoolService
import com.example.photondemo.ui.main.utls.HighSchoolRepository
import com.example.photondemo.ui.main.utls.ViewModelFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainFragment : Fragment() {
    private lateinit var binding: FragmentMainBinding
    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: HighSchoolViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        //may use a service manager class instead depends on the how many services
        val retrofit = Retrofit.Builder()
            .baseUrl("https://data.cityofnewyork.us/resource/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val service = retrofit.create(HighSchoolService::class.java)
        val repository = HighSchoolRepository(service)

        viewModel = ViewModelProvider(this, ViewModelFactory(repository))[HighSchoolViewModel::class.java]
        viewModel.getHighSchools().observe(this, Observer { highSchools ->
            if(highSchools.isNullOrEmpty()){
                //can build a custom error window instead
                Toast.makeText(requireContext(),"no such dbn or service error", Toast.LENGTH_SHORT).show()
            }else{
                val adapter = SchoolAdapter(highSchools,this)
                binding.schoolList.layoutManager = LinearLayoutManager(context).apply {
                    this.orientation = LinearLayoutManager.VERTICAL
                }
                binding.schoolList.adapter = adapter
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false)
        return binding.root
    }

}