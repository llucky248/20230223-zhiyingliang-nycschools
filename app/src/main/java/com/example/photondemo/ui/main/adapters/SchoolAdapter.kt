package com.example.photondemo.ui.main.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.example.photondemo.R
import com.example.photondemo.databinding.ItemSchoolBinding
import com.example.photondemo.ui.main.HighSchoolDetailFragment
import com.example.photondemo.ui.main.models.HighSchool

class SchoolAdapter(private val schools: List<HighSchool>, private val fragment: Fragment) : RecyclerView.Adapter<SchoolAdapter.SchoolViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemSchoolBinding>(inflater, R.layout.item_school, parent, false)
        return SchoolViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        val school = schools[position]
        holder.binding.textviewSchoolName.text = school.schoolName
        holder.binding.textviewSchoolAddress.text = school.location
        holder.binding.moreDetailBtn.setOnClickListener {
            school.dbn?.let{
                fragment.parentFragmentManager.beginTransaction()
                    .add(R.id.container, HighSchoolDetailFragment.newInstance(it))
                    .addToBackStack(javaClass.simpleName)
                    .commitAllowingStateLoss()
            }
        }
    }

    override fun getItemCount(): Int {
        return schools.size
    }
    class SchoolViewHolder(val binding: ItemSchoolBinding) : RecyclerView.ViewHolder(binding.root)
}
